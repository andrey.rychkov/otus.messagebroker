﻿using System;

namespace Otus.Teaching.Pcf.Messaging.Contracts
{
    public class NotifyAdminAboutPartnerManagerPromoCodeContract
    {
        public Guid PartnerManagerId { get; set; }
    }
}
