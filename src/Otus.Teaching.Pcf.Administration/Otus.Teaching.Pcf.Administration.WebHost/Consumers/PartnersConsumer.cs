﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core;
using Otus.Teaching.Pcf.Messaging.Contracts;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PartnerConsumer : IConsumer<NotifyAdminAboutPartnerManagerPromoCodeContract>
    {
        private readonly IPartnerManagerService _partnerManagerService;

        public PartnerConsumer(IPartnerManagerService partnerManagerService)
        {
            _partnerManagerService = partnerManagerService;
        }

        public async Task Consume(ConsumeContext<NotifyAdminAboutPartnerManagerPromoCodeContract> context)
        {
            await _partnerManagerService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}
