﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core
{
    public interface IPartnerManagerService
    {
        public Task UpdateAppliedPromocodesAsync(Guid id);
    }
}