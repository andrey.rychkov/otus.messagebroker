using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Messaging.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess;
using Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Data;
using Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Settings;
using System;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;


namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            
            var integrationSettings = Configuration.GetSection("IntegrationSettings").Get<IntegrationSettings>();

            services.AddSingleton(integrationSettings);

            services.Configure<IntegrationSettings>(Configuration.GetSection("IntegrationEndpoints"));

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<INotificationGateway, NotificationGateway>();
            services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddScoped<IAdministrationGateway, MessageBrokerAdministrationGateway>();
            services.AddScoped<IGivingPromoCodeToCustomerGateway, MessageBrokerGivingPromocodeToCustomerGateway>();


            services.AddMassTransit((x) =>
            {
                x.SetSnakeCaseEndpointNameFormatter();

                x.UsingRabbitMq((context, cfg) =>
                {

                    EndpointConvention.Map<GivePromoCodeToCustomerContract>(new Uri($"queue:{context.EndpointNameFormatter.Message<GivePromoCodeToCustomerContract>()}"));
                    EndpointConvention.Map<NotifyAdminAboutPartnerManagerPromoCodeContract>(new Uri($"queue:{context.EndpointNameFormatter.Message<NotifyAdminAboutPartnerManagerPromoCodeContract>()}"));

                    cfg.Host(integrationSettings.MessageBroker.RabbitMq.Host,
                        integrationSettings.MessageBroker.RabbitMq.VirtualHost,
                        h => {
                                h.Username(integrationSettings.MessageBroker.RabbitMq.UserName);
                                h.Password(integrationSettings.MessageBroker.RabbitMq.Password);
                        });

                    cfg.ConfigureEndpoints(context);
                });

            });



            services.AddDbContext<DataContext>(x =>
            {
                //x.UseSqlite("Filename=PromocodeFactoryReceivingFromPartnerDb.sqlite");
                x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryReceivingFromPartnerDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Receiving From Partner API Doc";
                options.Version = "1.0";
            });
         
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            dbInitializer.InitializeDb();
        }
    }
}