﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Settings
{
    public class IntegrationSettings
    {
        public string GivingToCustomerApiUrl { get; set; }
        public string AdministrationApiUrl { get; set; }

        public MessageBrokerSetting MessageBroker { get; set; } = new MessageBrokerSetting();

        public class MessageBrokerSetting
        {
            public RabbitMqSettings RabbitMq { get; set; } = new RabbitMqSettings();

            public class RabbitMqSettings
            {
                public string Host { get; set; }
                public string VirtualHost { get; set; }
                public string UserName { get; set; }
                public string Password { get; set; }
            }
        }
    }
}
