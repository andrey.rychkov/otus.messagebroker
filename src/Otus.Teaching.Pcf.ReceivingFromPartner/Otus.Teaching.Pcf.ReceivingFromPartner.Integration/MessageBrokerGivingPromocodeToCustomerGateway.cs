﻿using MassTransit;
using Otus.Teaching.Pcf.Messaging.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class MessageBrokerGivingPromocodeToCustomerGateway : IGivingPromoCodeToCustomerGateway
    {
        private readonly IBus _bus;

        public MessageBrokerGivingPromocodeToCustomerGateway(IBus bus )
        {
            _bus = bus;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {

            var msg = new GivePromoCodeToCustomerContract()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            await _bus.Send(msg);
        }
    }
}
