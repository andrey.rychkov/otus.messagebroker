﻿using MassTransit;
using Otus.Teaching.Pcf.Messaging.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class MessageBrokerAdministrationGateway : IAdministrationGateway
    {
        private readonly IBus _bus;

        public MessageBrokerAdministrationGateway(IBus bus)
        {
            _bus = bus;
        }

        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            var msg = new NotifyAdminAboutPartnerManagerPromoCodeContract()
            {
                PartnerManagerId = partnerManagerId
            };
            await _bus.Send<NotifyAdminAboutPartnerManagerPromoCodeContract>(msg);
        }
    }
}
