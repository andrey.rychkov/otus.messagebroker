﻿namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Settings
{
    public class MessageBrokerSetting
    {
        public RabbitMqSettings RabbitMq { get; set; } = new RabbitMqSettings();

        public class RabbitMqSettings
        {
            public string Host { get; set; }
            public string VirtualHost { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }
}
