﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using Otus.Teaching.Pcf.Messaging.Contracts;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumer
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerContract>
    {
        private readonly IGivePromoCodeToCustomerService _givePromoCodeToCustomerService;

        public GivePromoCodeToCustomerConsumer(IGivePromoCodeToCustomerService givePromoCodeToCustomerService)
        {
            _givePromoCodeToCustomerService = givePromoCodeToCustomerService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerContract> context)
        {
            var msg = new GivePromoCodeToCustomerServiceMessage
            {
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate,
                PartnerId = context.Message.PartnerId,
                PreferenceId = context.Message.PreferenceId,
                PromoCode = context.Message.PromoCode,
                PromoCodeId = context.Message.PromoCodeId,
                ServiceInfo = context.Message.ServiceInfo
            };
            
            await _givePromoCodeToCustomerService.GivePromoCodesToCustomersWithPreferenceAsync(msg);
        }
    }
}
