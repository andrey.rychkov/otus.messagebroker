﻿
using System;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IGivePromoCodeToCustomerService
    {
        public Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerServiceMessage message);
    }
}